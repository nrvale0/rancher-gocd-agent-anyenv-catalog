# GoCD agent w/ anyenv (Experimental)
<https://github.com/nrvale0/rancher-gocd-agent-anyenv-catalog>
### Info:

 Create a GoCD agent with the following pre-installed:
 * anyenv
 * rbenv w/ Ruby 2.3.0
 * pyenv with Python 3.4.0
 * goenv with Go 1.6
 
### Usage:

 You'll need to select the desired GoCD Server with which you'd like the Agent to register.
 
### TODO
  * provide environment variables to allow for automated registration of the Agent with a GoCD Server.
